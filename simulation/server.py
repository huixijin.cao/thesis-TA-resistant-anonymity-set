# -*- coding: utf-8 -*-

import datetime
import json

from apscheduler.schedulers.background import BackgroundScheduler
from flask import request

from . import app
from simulation.models import User_info, db
from simulation.encrypt_message import decrypt_message

db.create_all()


def sensor():
    """ Function for test purposes. """
    print("Scheduler is alive!")


sched = BackgroundScheduler(daemon=True)
sched.add_job(sensor, "interval", minutes=1)
sched.start()


@app.route("/api/user_info", methods=["POST"])
def user_info():
    """ Function for parsing user info"""
    data = request.data
    try:
        data = decrypt_message(data)
        batches = json.loads(data)
        for batch in batches:
            line = json.loads(batch)
            timestamp = datetime.datetime.fromtimestamp(line["timestamp"])
            user_id = line["user_id"]
            user = User_info(timestamp=timestamp, user_id=user_id)
            db.session.merge(user)
        db.session.commit()
    except Exception as e:
        return {"status": 0, "error": str(e)}
    return {"status": 1}

