# -*- coding: utf-8 -*-

from apscheduler.schedules.background import BackgroundScheduler
from flask_apscheduler import APScheduler

from . import app


class SchedulerConfig(object):
    JOBS = [
        {
            "id": "",
            "func": None,
            "args": None,
            "trigger": {
                "type": "cron",
                # 'day_of_week': "0-6", # Define specific days to execute
                "hour": "*/1",
                # "minute": "*/15",
                # 'second': '*/5'  # "*/3" ,execute every 3 seconds
            },
        }
    ]
    SCHEDULER_API_ENABLED = True
