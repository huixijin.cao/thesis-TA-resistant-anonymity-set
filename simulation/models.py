# -*- coding: utf-8 -*-

from datetime import datetime

from flask_sqlalchemy import SQLAlchemy

from . import app

db = SQLAlchemy(app)


class User_info(db.Model):

    user_id = db.Column(db.String, primary_key=True)
    timestamp = db.Column(db.DateTime, primary_key=True)



class Cluster_result(db.Model):

    timestamp = db.Column(db.DateTime, unique=True, primary_key=True)
    normal_user = db.Column(db.Text)
    abnormal_user = db.Column(db.Text)
    normal_user_count = db.Column(db.Integer, default=0)
    abnormal_user_count = db.Column(db.Integer, default=0)
    nyms_count = db.Column(db.Integer, default=0)


class Nym_info(db.Model):

    nym = db.Column(db.String, primary_key=True, unique=True)
    timestamp = db.Column(db.DateTime, primary_key=True)
