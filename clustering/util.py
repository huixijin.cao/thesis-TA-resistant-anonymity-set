
import pandas as pd

class Constants:
    CLUSTER_NUM = 100
    ROUND_LENGTH = 3600        # in seconds
    CLUSTER_SIZE = 30       # minimum cluster size that must be satisfied by forwarding messages
    NUM_COVER = 1
    MAX_DELAY = 3
    ENCODING = 'UTF-8'

def cnt_cluster(model):
    """Count number of users in each cluster"""
    cnt = pd.Series(model.labels_).value_counts().to_frame()
    cnt.columns = ['count']
    print('Non_empty cluster number:', len(cnt))
    print('The number of users in each cluster is：\n', cnt)
    return cnt






