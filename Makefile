.PHONY: format
format:
	pre-commit run --all-files
	python -m black .
